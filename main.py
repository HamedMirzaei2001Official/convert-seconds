"""create a function for convert second to minute/hour/day
"""
def convert(second):
    if not second > 0:
        return "second must be > 0"

    hour = int()
    day = int()

    minute = second / 60
    second = second % 60
    if minute > 60:
        hour = minute / 60
        minute = minute % 60

    if hour > 24:
        day = hour / 24
        hour = hour % 24

    return [int(day), int(hour), int(minute), int(second)]


if __name__ == "__main__":
    second = int(input())
    print(convert(second))